package topn

import (
	"bufio"
	"io"
	"os"
	"strconv"
)

func findNMaxNumbersInFile(count int, file io.Reader) ([]int, error) {
	scanner := bufio.NewScanner(file)
	if err := scanner.Err(); err != nil {
		return nil, err
	}

	h := &IntHeap{}

	for scanner.Scan() {
		number, err := strconv.Atoi(scanner.Text())
		if err != nil {
			return nil, err
		}

		h.PushOrReplaceMin(number, count)
	}

	return h.Sorted(), nil
}

// Maximum returns a sorted slice containing the count biggest
// numbers in the file found under path
func FindNMaxNumbersInFile(count int, path string) ([]int, error) {
	file_reader, err := os.Open(path)
	if err != nil {
		return nil, err
	}

	return findNMaxNumbersInFile(count, file_reader)
}
