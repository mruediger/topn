# TopN

TopN searches for the N largest numbers in a file and outputs them. It makes use of `bufio.Scanner` to allow arbitrarily large files to be read. Though it uses an efficient data structure to store and compare the numbers it found, it still holds them in memory and is therefore limited by its size.

## Complexity

TopN internally uses a min heap to collect the results and thus has a time complexity of $`O(n \log m)`$ where $`n`$ is the number of integers in the file and $`m`$ is the number of results requested. The fixed buffer size of `bufio.Scanner` and the limited number of results stored in the heap mean that the space complexity is constant and only depending on the number of results requested.

## Possible improvements

The performance is largely governed by the read performance of the file. Minor improvements might be made by adjusting the buffer size of `bufio.Scanner`. If the number of results requested is sufficiently big, it could also be beneficial to provide a custom heap implementation that handles the size constraint internally during the bubble up or down phase.
Decoupling and parallelizing this phase by using channels might also yield minor improvements.

To properly speed up the performance, the read has to be parallelized though. This can be done by copying parts of the file across multiple devices or better across multiple machines and running multiple instances of TopN. Merging the results afterwards, since they are sorted, is trivial.
