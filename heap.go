package topn

import (
	"container/heap"
	"sort"
)

// An IntHeap is a min-heap of ints.
type IntHeap []int

func (h IntHeap) Len() int           { return len(h) }
func (h IntHeap) Less(i, j int) bool { return h[i] < h[j] }
func (h IntHeap) Swap(i, j int)      { h[i], h[j] = h[j], h[i] }

func (h *IntHeap) Push(x interface{}) {
	*h = append(*h, x.(int))
}

func (h *IntHeap) Pop() interface{} {
	old := *h
	n := len(old)
	x := old[n-1]
	*h = old[0 : n-1]
	return x
}

// Min returns the smallest integer out of the min-heap
func (h IntHeap) Min() int { return h[0] }

// PushOrReplaceMin fills the heap to the specified max_capacity. Once
// it is reached, every subsequent call of the method replaces the smallest
// element of the heap with number if it is bigger and does nothing otherwise.
func (h *IntHeap) PushOrReplaceMin(number, max_capacity int) {
	if h.Len() < max_capacity || h.Min() < number {
		heap.Push(h, number)
	}

	if h.Len() > max_capacity {
		heap.Pop(h)
	}
}

// Sorted returns a sorted copy of the heaps contents
func (h IntHeap) Sorted() []int {
	sorted := append(h[:0:0], h...)
	sort.Slice(sorted, func(i, j int) bool { return sorted[i] > sorted[j] })
	return sorted
}
