package main

import (
	"flag"
	"fmt"
	"log"
	"os"

	topN "gitlab.com/mruediger/topN"
)

var count int

func init() {
	flag.IntVar(&count, "count", 10, "number of largest values to output")
}

func main() {
	flag.Usage = usage
	flag.Parse()
	args := flag.Args()

	if len(args) != 1 {
		usage()
		return
	}

	filename := args[0]

	max_numbers, err := topN.FindNMaxNumbersInFile(count, filename)
	if err != nil {
		log.Fatal(err)
	}

	for _, number := range max_numbers {
		fmt.Println(number)
	}
}

func usage() {
	fmt.Fprintf(flag.CommandLine.Output(), "Usage: %s [OPTION]... [FILE]\n\n", os.Args[0])
	flag.PrintDefaults()
}
