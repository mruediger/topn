package topn

import (
	"strconv"
	"strings"
	"testing"
)

var test_numbers = []string{
	"31915",
	"31927",
	"31986",
	"32033",
	"32037",
	"32044",
	"32091",
	"32144",
	"32385",
	"32385",
	"32421",
	"32485",
	"32492",
	"32502",
	"32549",
	"32587",
	"32593",
	"32642",
	"32685",
	"32702",
}

func TestMaximum(t *testing.T) {
	reader := strings.NewReader(strings.Join(test_numbers, "\n"))

	numbers, err := findNMaxNumbersInFile(10, reader)
	if err != nil {
		t.Error(err)
	}

	for i, number := range numbers {
		got := number
		wanted, _ := strconv.Atoi(test_numbers[len(test_numbers)-i-1])

		if got != wanted {
			t.Errorf("picked wrong numbers: %d != %d", got, wanted)
		}

	}
}
