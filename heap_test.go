package topn

import (
	"container/heap"
	"math/rand"
	"testing"
)

func TestMaxCapacity(t *testing.T) {
	h := &IntHeap{}

	capacity := 5

	for i := 0; i < 20; i++ {
		h.PushOrReplaceMin(rand.Int(), capacity)
		if h.Len() > capacity {
			t.Errorf("heap grew out of proportion")
		}
	}
}

func TestMinReplaced(t *testing.T) {
	h := &IntHeap{}

	capacity := 5

	for i := 0; i < 20; i++ {
		h.PushOrReplaceMin(i, capacity)
	}

	for i := 15; i < 20; i++ {
		if x := heap.Pop(h); x != i {
			t.Errorf("heap contained wrong element [%d] != %d", x, i)
		}
	}
}

func equals(a, b []int) bool {
	for i, v := range a {
		if v != b[i] {
			return false
		}
	}
	return true
}

func TestSortedIsIdempotent(t *testing.T) {
	h := &IntHeap{}

	for i := 5; i > 0; i-- {
		heap.Push(h, i)
	}

	x := h.Sorted()

	if equals(x, *h) {
		t.Errorf("Sorted() is not idempotent")
	}
}
